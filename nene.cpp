///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene(string newtag, enum Color newColor, enum Gender newGender) {

/*      if (newisNative == 1)
                nativity = "true";
        if (newisNative == 0)
                nativity = "false";

	if (newisMigratory == true)
		boo = "true";
	if (newisMigratory == false)
		boo = "false";
*/
        tag = newtag;
        gender = newGender;         /// Get from the constructor... not all cats are the same gender (this is a has-a relationship)
        species = "Branta sandvicensis";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
        featherColor = newColor;       /// A has-a relationship, so it comes through the constructor
        isMigratory = true;         /// An is-a relationship, so it's safe to hardcode.  All cats have the same gestation period.
//        favoriteTemp = newfavoriteTemp;             /// A has-a relationship.  Every cat has its own name.

}


const string Nene::speak() {
        return string( "Nay, Nay" );
}


/// Print our Cat and name first... then print whatever information Mammal holds.
void Nene::printInfo() {
        cout << "Nene"  << endl;
        cout << "   Tag ID = [" << tag << "]" << endl;
        Bird::printInfo();
}

} // namespace animalfarm

