///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila( string newplace, enum Color newColor, enum Gender newGender) {

        isMigratory = false;
/*      if (newisNative == 1)
                nativity = "true";
        if (newisNative == 0)
                nativity = "false";

	if (newisMigratory == true)
                boo = "true";
        if (newisMigratory == false)
                boo = "false";
*/
	place = newplace;
        gender = newGender;         /// Get from the constructor... not all cats are the same gender (this is a has-a relationship)
        species = "Loxioides bailleui";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
        featherColor = newColor;       /// A has-a relationship, so it comes through the constructor
//        favoriteTemp = newfavoriteTemp;             /// A has-a relationship.  Every cat has its own name.

}



/// Print our Cat and name first... then print whatever information Mammal holds.
void Palila::printInfo() {
        cout << "Palila"  << endl;
        cout << "   Where Found = [" << place << "]" << endl;
        Bird::printInfo();
}

} // namespace animalfarm

