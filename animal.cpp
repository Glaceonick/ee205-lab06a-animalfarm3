///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

/*

Animal::Animal() {
	cout << "." ;
}


Animal::~Animal() {
	cout << "x" ;
}

*/

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {

	/// @todo Implement this based on genderName and your work
	///       on Animal Farm 1
	
	switch (color) {
		case BLACK:	return string("Black"); break;
		case WHITE:     return string("White"); break;
		case RED:	return string("Red"); break;
		case SILVER:	return string("Silver"); break;
		case YELLOW:	return string("Yellow"); break;
		case BROWN:     return string("Brown"); break;
			}


   return string("Unknown");
};

const bool Animal::getRandomBool()
{
        srand (time(NULL));
        int ran;
        ran = rand()%2;
        switch (ran) {
                case 0: return true; break;
                case 1: return false; break;
        }
        return 0;
};

const string Animal::getRandomName()
{
        srand (time(NULL));

        char letter[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','z'};

        int ran;
        int i;
        char c;
        int length = (rand()%6)+5;
        string name;

        for (i=1;i<length;i++)
                {
                        ran = rand()%26;
                        name = name + letter[rand()%25];
                }
        return string(name);
};

const float Animal::getRandomWeight()
{
        float ran;

        srand (time(NULL));
        ran = rand()%9999;
        ran = ran/100;
        return ran;

};

const Gender Animal::getRandomGender()
{
        srand (time(NULL));
        int ran;
        ran = rand()%2;
        switch (ran) {
                case 0: return MALE; break;
                case 1: return FEMALE; break;
		}
	return MALE;
};

const enum Color Animal::getRandomColor()
{
        srand (time(NULL));
        int ran;
        ran = rand()%6;
        switch (ran) {
                case 0: return BLACK; break;
                case 1: return WHITE; break;
                case 2: return RED; break;
                case 3: return SILVER; break;
                case 4: return YELLOW; break;
                case 5: return BROWN; break;
        }
	return BLACK;
};

/*
Animal AnimalFactory::getRandomAnimal() {
	Animal* newanimal = NULL;
	int i = getRandomInteger % 6;
	switch (i) {
		case 0: newAnimal = new Cat	(getRandomName, getRandomColor, getRandomGender);
		case 1: newAnimal = new Dog	(getRandomName, getRandomColor, getRandomGender);
		case 2: newAnimal = new Nunu	(getRandomBool, RED, getRandomGender);
		case 3: newAnimal = new Aku	(getRandomWeight, SILVER, getRandomGender);
		case 4: newAnimal = new Palila	(getRandomName, YELLOW, getRandomGender);
		case 5: newAnimal = new Nene	(getRandomName, BROWN, getRandomGender);

			return newAnimal;
	}
};
*/

} // namespace animalfarm
