///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN };  /// @todo Add more colors

//enum Native { true, false };

class Animal {
public:
	enum Gender gender;
	string      species;
//	bool isNative;
	virtual const string speak() = 0;
	
	void printInfo();

//	string nativity;	
	string colorName  (enum Color color);
	string genderName (enum Gender gender);

	static const Gender     getRandomGender();
	static const Color      getRandomColor();
	static const bool       getRandomBool();
	static const float      getRandomWeight();
	static const std::string getRandomName();


};


} // namespace animalfarm
