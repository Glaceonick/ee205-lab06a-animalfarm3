///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku( float newweight, enum Color newColor, enum Gender newGender) {

	weight = newweight;
//      isNative = newisNative;
/*      if (newisNative == 1)
                nativity = "true";
        if (newisNative == 0)
                nativity = "false";
*/
        //scale = 1;
        gender = newGender;         /// Get from the constructor... not all cats are the same gender (this is a has-a relationship)
        species = "Katsuwonus pelamis";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
        scaleColor = newColor;       /// A has-a relationship, so it comes through the constructor
        //gestationPeriod = 0;         /// An is-a relationship, so it's safe to hardcode.  All cats have the same gestation period.
        favoriteTemp = 75;             /// A has-a relationship.  Every cat has its own name.

}


void Aku::printInfo() {
        cout << "Aku"  << endl;
        cout << "   Weight = [" << weight << "]" << endl;
        Fish::printInfo();
}

} // namespace animalfarm

