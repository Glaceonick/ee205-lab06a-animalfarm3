
#include <unistd.h>
#include <iostream>
using namespace std;

float getRandomWeight()
{
        float ran;

        srand (time(NULL));
        ran = rand()%9999;
        ran = ran/100;
        return ran;

}

string getRandomGender()
{
        srand (time(NULL));
        int ran;
        ran = rand()%2;
        switch (ran) {
                case 0: return string("MALE"); break;
                case 1: return string("FEMALE"); break;
        }
        return 0;
};

string getRandomColor()
{
        srand (time(NULL));
        int ran;
        ran = rand()%6;
        switch (ran) {
                case 0: return string("BLACK"); break;
                case 1: return string("WHITE"); break;
                case 2: return string("RED"); break;
                case 3: return string("SILVER"); break;
                case 4: return string("YELLOW"); break;
                case 5: return string("BROWN"); break;
        }
	return 0;
};

string getRandomBool()
{
        srand (time(NULL));
        int ran;
        ran = rand()%2;
        switch (ran) {
                case 0: return string("true"); break;
                case 1: return string("false"); break;
	}
	return 0;
};

string getRandomName()
{
	srand (time(NULL));

	char letter[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','z'};

	int ran;
	int i;
	char c;
	int length = (rand()%6)+5;
	string name;

	for (i=1;i<length;i++)
		{
			ran = rand()%26;
			name = name + letter[rand()%25];
		}	
	return name;
}


int main()
{

    cout << getRandomWeight() << endl;
    cout << getRandomGender() << endl;
    cout << getRandomColor() << endl;
    cout << getRandomBool() << endl;
    cout << getRandomName() << endl;

    usleep(1000000);

    cout << getRandomName() << endl;

}

// Random Generators //

/*
string getRandomGender() 
{
        srand (time(NULL));
        int ran;
        ran = rand()%1;
        switch (ran) {
                case 0: return string("MALE"); break;
                case 1: return string("FEMALE"); break;
        }
	return 0;
};


string getRandomColor 
{
        srand (time(NULL));
        int ran;
        ran = rand()%5;
        switch (ran) {
                case 0: return string("BLACK"); break;
                case 1: return string("WHITE"); break;
                case 2: return string("RED"); break;
                case 3: return string("SILVER"); break;
                case 4: return string("YELLOW"); break;
                case 5: return string("BROWN"); break;
        }
};

string getRandomBool 
{
        srand (time(NULL));
        int ran;
        ran = rand()%5;
        switch (ran) {
                case 0: return string("true"); break;
                case 1: return string("false"); break;
};

float getRandomWeight 
{
        float ran;

        srand (time(NULL));
        ran = rand()%9999;
        ran = ran/100;
        return ran;

}

string getRandomName 
{
return 0;
}
*/
