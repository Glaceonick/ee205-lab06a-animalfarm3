///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.hpp
/// @version 1.0
///
/// Exports data about all nene
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once


#include "bird.hpp"



namespace animalfarm {

class Nene : public Bird {
public:

        Nene( string newtag, enum Color newColor, enum Gender newGender );

	string tag;
        virtual const string speak();

        void printInfo();
};

} // namespace animalfarm

