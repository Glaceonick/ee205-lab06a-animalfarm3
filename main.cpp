///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Nicholas Tom <tom7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <stdlib.h>
#include <array>
#include <memory>
#include <list>


#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "animalFactory.hpp"

using namespace std;
using namespace animalfarm;

/*
float getRandomWeight()
{
        float ran;

        srand (time(NULL));
        ran = rand()%9999;
        ran = ran/100;
        return ran;

};

string getRandomGender()
{
        srand (time(NULL));
        int ran;
        ran = rand()%2;
        switch (ran) {
                case 0: return string("MALE"); break;
                case 1: return string("FEMALE"); break;
        }
        return 0;
};

string getRandomColor()
{
        srand (time(NULL));
        int ran;
        ran = rand()%6;
        switch (ran) {
                case 0: return string("BLACK"); break;
                case 1: return string("WHITE"); break;
                case 2: return string("RED"); break;
                case 3: return string("SILVER"); break;
                case 4: return string("YELLOW"); break;
                case 5: return string("BROWN"); break;
        }
        return 0;
};

string getRandomBool()
{
        srand (time(NULL));
        int ran;
        ran = rand()%2;
        switch (ran) {
                case 0: return string("true"); break;
                case 1: return string("false"); break;
        }
        return 0;
};

string getRandomName()
{
        srand (time(NULL));

        char letter[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','z'};

        int ran;
        int i;
        char c;
        int length = (rand()%6)+5;
        string name;

        for (i=1;i<length;i++)
                {
                        ran = rand()%26;
                        name = name + letter[rand()%25];
                }
        return string(name);
};


getRandomAnimal() {
        Animal* newanimal = NULL;
        int i = getRandomInteger % 6;
        switch (i) {
                case 0: newAnimal = new Cat     (getRandomName, getRandomColor, getRandomGender);
                case 1: newAnimal = new Dog     (getRandomName, getRandomColor, getRandomGender);
                case 2: newAnimal = new Nunu    (getRandomBool, RED, getRandomGender, getRandomWeight);
                case 3: newAnimal = new Aku     (getRandomWeight, SILVER, getRandomGender, 75);
                case 4: newAnimal = new Palila  (getRandomName, YELLOW, getRandomGender);
                case 5: newAnimal = new Nene    (getRandomName, BROWN, getRandomGender);

                        return newAnimal;
        }
};
*/

int main() {
	cout << "Welcome to Animal Farm 3" << endl;
	cout << endl << "Please wait 20secs to generate farm" << endl;
	/*	
	//Cat myCat( getRandomName(), getRandomColor(), getRandomGender() );
	Cat myCat( getRandomName(), BLACK, FEMALE );
	myCat.printInfo();
	
	Dog myDog( getRandomName(), WHITE, MALE );
	myDog.printInfo();


	Nunu myNunu( true, RED, FEMALE);
	myNunu.printInfo();
	
	Aku myAku( 15.0, SILVER, MALE);
	myAku.printInfo();


	Palila myPalila( "Kapiolani Regional Park", YELLOW, FEMALE );
	myPalila.printInfo();
	
	Nene myNene( "2202-A-802.1", BROWN, FEMALE );
	myNene.printInfo();



#define SIZE_OF_FARM (6)
	
	Animal* myAnimals[ SIZE_OF_FARM ];


	for( int i = 0 ; i < SIZE_OF_FARM ; i++ ) {
		myAnimals[i] = getRandomAnimal();
	}

	
	myAnimals[0] = &myCat;
	myAnimals[1] = &myDog;
	myAnimals[2] = &myNunu;
	myAnimals[3] = &myAku;
	myAnimals[4] = &myPalila;
	myAnimals[5] = &myNene;


	cout << endl;
	cout << "Here's what it sounds like around the farm:" << endl;
	for( int i = 0 ; i < SIZE_OF_FARM ; i++ ) {
		if( myAnimals[i] != NULL) {
			cout << "   " << myAnimals[i]->speak() << endl;
		}
	}
	
	return 0;
*/

		array<Animal*, 30> animalArray;
	animalArray.fill( NULL );

	for( auto i = 0 ; i < 25 ; i++ ) {
		animalArray[i] = AnimalFactory::getRandomAnimal();
	}

	cout << endl;
	cout << "Array of Animals" << endl;
	cout << "  Is it empty: " << boolalpha << animalArray.empty() << endl;
	cout << "  Number of elements: " << animalArray.size() << endl;
	cout << "  Max size: " << animalArray.max_size() << endl;

	for( Animal* animal : animalArray ) {
		if( animal != NULL )
			cout << animal->speak() << endl;
	}


	list<Animal*> animalList;

	for( auto i = 0 ; i < 25 ; i++ ) {
		animalList.push_front( AnimalFactory::getRandomAnimal() ) ;
	}

	cout << endl;
	cout << "List of Animals" << endl;
	cout << "  Is it empty: " << boolalpha << animalList.empty() << endl;
	cout << "  Number of elements: " << animalList.size() << endl;
	cout << "  Max size: " << animalList.max_size() << endl;

	for( Animal* animal : animalList ) {
		cout << animal->speak() << endl;
	}
	return 0;

}
