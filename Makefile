###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author Nicholas Tom <tom7@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   03_01_2021
###############################################################################

all: main

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

bird.o: bird.hpp bird.cpp
	g++ -c bird.cpp

fish.o: fish.hpp fish.cpp
	g++ -c fish.cpp
	
nunu.o: nunu.hpp nunu.cpp
	g++ -c nunu.cpp
	
aku.o: aku.hpp aku.cpp
	g++ -c aku.cpp
	
nene.o: nene.hpp nene.cpp
	g++ -c nene.cpp
	
palila.o: palila.hpp palila.cpp
	g++ -c palila.cpp
	
dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp
	
cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp

animalFactory.o: animalFactory.cpp *.hpp
	g++ -c animalFactory.cpp

main: main.cpp *.hpp main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o animalFactory.o
	g++ -o main main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o animalFactory.o
	
clean:
	rm -f *.o main

